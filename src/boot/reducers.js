import { combineReducers } from "redux";
import showListReducer from "../components/common/organisms/ShowList/ShowList.reducer";

export default combineReducers({
  showList: showListReducer
});
