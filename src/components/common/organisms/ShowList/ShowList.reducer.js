// import { FETCH_SHOW_LIST } from "./ShowList.actions";

export default (state = null, action) => {
  switch (action.type) {
    case "FETCH_SHOW_LIST":
      return action.payload.data;
    default:
      return state;
  }
};
