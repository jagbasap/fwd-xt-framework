import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import showlist from "./ShowList.reducer";

const showListStore = createStore(showlist, applyMiddleware(thunk));
export default showListStore;
