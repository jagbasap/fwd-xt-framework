// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import { Text } from "@sitecore-jss/sitecore-jss-react";
import { fetchShowList } from "./ShowList.actions";

class ShowList extends Component {
  componentDidMount() {
    // console.log(`component did mount`);
    this.props.fetchShowList();
  }

  render() {
    return (
      <div>
        <Text
          tag="h2"
          className="display-4"
          field={this.props.fields.heading}
        />
        <ul>
          {this.props.shows &&
            this.props.shows.map(({ show }) => (
              <li key={show.id}>
                <h2>{show.name}</h2>
              </li>
            ))}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    showList: state.shows
  };
}

function loadData(store) {
  return store.dispatch(fetchShowList());
}

export default {
  loadData,
  component: connect(
    mapStateToProps,
    { fetchShowList }
  )(ShowList)
};
